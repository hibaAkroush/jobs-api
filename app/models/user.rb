class User < ApplicationRecord
  has_many :applications
  has_many :jobs, through: :applications

  # Validations
  validates_presence_of :email, :password
end

# has_many :applications, foreign_key: : ???
