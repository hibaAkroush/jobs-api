require 'rails_helper'

RSpec.describe User, type: :model do
  it { should validate_presence_of(:email) }
  # changed from password after creating user modle
  it { should validate_presence_of(:password) }
end
