require 'rails_helper'

RSpec.describe Job, type: :model do
  it { should validate_presence_of(:title) }
  it { should validate_presence_of(:description) }
end

# spec for user

# require 'rails_helper'

# RSpec.describe User, type: :model do
# 	# users and jobs have many to many relationship
# 	it { should have_many(:applications) }

# 	it { should validate_presence_of(:email) }
# 	#changed from password after creating user modle
# 	it { should validate_presence_of(:password_digest) }
# end
