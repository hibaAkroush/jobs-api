class Job < ApplicationRecord
  # model association
  has_many :applications
  has_many :users, through: :applications

  # validations
  validates_presence_of :title, :description
end
