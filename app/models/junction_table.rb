class JunctionTable < ApplicationRecord
  belongs_to :user
  belongs_to :job
  validates_presence_of :seen
end
