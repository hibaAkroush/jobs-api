class CreateJobs < ActiveRecord::Migration[5.1]
  def change
    create_table :jobs, &:timestamps
  end
end
